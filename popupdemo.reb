REBOL [
    Title: {Primordial Popup Window via libRebol in JavaScript}
    Description: {See README.md}
    Type: module
    Name: PopupDemo
]

css-do <popupdemo.css>
js-do <popupdemo.js>

export show-dialog: js-native [html [text!] /title [text!] /size [pair!]] {
    let title = reb.Spell("any [", reb.Q(reb.ArgR('title')), "{}]")
    let width = reb.UnboxInteger("any [(pick", reb.Q(reb.ArgR('size')), "1) 400]")
    let height = reb.UnboxInteger("any [(pick", reb.Q(reb.ArgR('size')), "2) 200]")
    setDialog("open", {
        title: title,
        width: width,
        height: height,
        content: reb.Spell(reb.ArgR('html')),
        buttons: {}
        /*buttons: {
            "Delete": function() {
                setDialog("open", {
                    title: "Confirmation",
                    content: "Are you sure?",
                    overlay: true,
                    buttons: {
                        "Yes": function() {
                            setDialog("close")
                        },
                        "No": function() {
                            alert("Canceled!")
                            setDialog("close")
                        }
                    }
                })
            }
        }*/
    })
}

export hide-dialog: js-native [] {
    setDialog("close")
}

