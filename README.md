# popupdemo

This was the first ever attempt at a "proof-of-concept" of showing a JavaScript
popup window with libRebol.js.  In order to not require a giant framework to do
so, an extremely minimal plain JS example of a floating dialog box was picked:

https://codepen.io/tovic/pen/XJEONy

*(If someone really wanted to develop a VID-like system, they would likely
benefit from a framework.  I just didn't know what a good choice would be, so
avoided making any choice.)*

What this is really for is just to explore fundamentals of a windowed UI and
how it might be involved with things like infinite loops, halting, and other
cross-cutting concerns.

To execute it from ReplPad-JS, type:

    do <popupdemo>

It should fetch the Rebol, CSS, and JS code via CORS requests from the URL
that popupdemo has been mapped to in the table DO consults.  Improved methods
of lookup for experiements and scripts are under development.
